<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\saytu;
use App\Http\Controllers\Traf;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/main', [App\Http\Controllers\HomeController::class, 'gif'])->name('main');

Route::get('/sort', [App\Http\Controllers\HomeController::class, 'poi'])->name('sort');

Route::middleware(['auth:sanctum', 'verified'])->get('/main', function () {
    return view('main');
})->name('main');

//Route::get('/main', function (){
//    return view('main');
//});
Route::get('main',[saytu::class, 'show']);

Route::get('sort',[Traf::class,'inde']);
