<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\sayt;

class Traf extends Controller
{
    public function inde()
    {
        $tr = sayt::select('zhangre', 'Domen', 'Vidvidyvachi', 'Trafik')
            ->orderBy('Trafik', 'asc')
            ->get();
        return view('sort',['eby'=>$tr]);
    }
}
